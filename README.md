# API NASA

Cette app a été crée avec l'api Nasa-apod.

## Fonction

L'application affiche une image issue des bases de données de l'Agence spatiale Américaine avec une description de celle-ci.
L'image change quotidiennement.

### Comment ?

En effectuant un appel à l'API apod de la NASA, la réponse est récupérée en format JSON puis afficher dans un template HTML.

## Lien

Voici le lien vers l'APi APOD (Astronomy Picture of the Day): https://api.nasa.gov/#browseAPI
