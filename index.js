const display = document.querySelector(".display");

const fetchData = async () => {
  try {
    const response = await fetch(
      "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY ",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(),
      }
    );
    const data = await response.json();
    console.log(data);
    createApp(data);
  } catch (e) {
    console.log(e);
  }
};

fetchData();
function createApp(data) {
  const cardHTML = `   <div class="card">     
    <h2>${data.title}</h2>
    <img src = "${data.url}" />
    <p>${data.explanation}</p>     
    </div>
       `;

  display.innerHTML = cardHTML;
}
